# docker-centos7i386

Docker container containing CentOS 7 i386 root filesystem and packages.

Best run within CentOS 7:

```bash
# Install dependencies
sudo yum -y install install rsync docker lorax anaconda-tui yum-langpacks virt-install libvirt-python gtk3
sudo systemctl start docker

# Build xz
sudo ./containerbuild.sh centos7i386.ks

# Build docker image
pushd /var/tmp/containers/$(date +%Y%m%d)/centos-7i386/docker/
docker build -t harbottle/centos7i386:7.x .
```

Modified from https://github.com/CentOS/sig-cloud-instance-build/tree/master/docker (modified ks file forces i386 repos when hardware is x86_64).